%% graph should be generated using PlantUML
%% http://plantuml.com/

@startuml class-diagram-scaffold
!include style.pu

HomeManager o-- GroupManager
HomeManager *-- Node

Node : void show(Interest *)
Node : void route(Name, InterestHandler*)
Node *-- BootstrapClient
Node *-- BootstrapManager

BootstrapClient : void bootstrap(PairingCode)
BootstrapManager : void bootstrap(Name, PairingCode)

GroupManager : list<Data> getGroupKey(TimeStamp&, ...)
GroupManager : void addSchedule(string &name, Schedule&)
GroupManager : void addMember(ScheduleName&, Certificate&)

ProducerClient *-- Node
ProducerClient *-- Producer

Producer : Name createContentKey(TimePoing&, ...)
Producer : void produce(Data&, TimePoint&, ...)

ConsumerClient *-- Node
ConsumerClient *-- Consumer

Consumer : void consume(Name& data, ConsumptionCallBack&, ...)
Consumer : void setGroup(Name&)
Consumer : void addDecryptionKey(Name&, Buffer&)

abstract class DataProcessor
abstract class InterestPreProcessor
abstract class InterestPostProcessor

Node o-- DataProcessor
Node o-- InterestPreProcessor
Node o-- InterestPostProcessor

DataProcessor : Data process(Data &)
DataProcessor <|-- DataValidator
DataProcessor <|-- DefaultDataSigner
DataProcessor <|-- DecryptProcessor
DataProcessor <|-- Deserializer

InterestPreProcessor : void onInterest(Interest&, ...)
InterestPreProcessor <|-- InterestSignatureValidator
InterestPreProcessor <|-- ParameterValidator
InterestPostProcessor <|-- DefaultInterestSigner
InterestPostProcessor : void beforeSending(Interest &, ...)
@enduml